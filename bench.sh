#!/bin/bash
cabal install -O2 --constraint=type-level-natural-number==1.1   --program-suffix=-1.1
cabal install -O2 --constraint=type-level-natural-number==1.1.1 --program-suffix=-1.1.1
~/.cabal/bin/type-level-natural-number-bench-1.1   -gu 1.1.csv
~/.cabal/bin/type-level-natural-number-bench-1.1.1 -gu 1.1.1.csv
cat 1.1.csv   | cut -d, -f 2 | tail -n+3 > 1.1.dat
cat 1.1.1.csv | cut -d, -f 2 | tail -n+3 > 1.1.1.dat
cat <<-"PLOT" | gnuplot
	set xlabel "number :: N"
	set ylabel "time (nanoseconds)"
	set title "type-level-natural-number"
	set log
	set term png
	set output "bench.png"
	plot "./1.1.dat" using (10**$0):(1e9*$1) title "1.1" with lines, "./1.1.1.dat" using (10**$0):(1e9*$1) title "1.1.1" with lines
PLOT
display bench.png
