{-# LANGUAGE Rank2Types, ScopedTypeVariables #-}
module Main where

import Wrapper
import TypeLevel.NaturalNumber
import Criterion.Main

at :: forall r . Int -> (forall n . NaturalNumber n => n -> r) -> r
at i f = go (undefined :: Zero) i
  where
    go :: NaturalNumber m => m -> Int -> r
    go z 0 = f (undefined `asTypeOf` z)
    go z i = go (successorTo z) (pred i)

nat :: Int -> IO Int
nat i = at i naturalNumberAsIntIO

main :: IO ()
main = defaultMain
  [ bgroup "naturalNumberAsInt"
    [ bench "0" $ nat 0
    , bench "1" $ nat 1
    , bench "10" $ nat 10
    , bench "100" $ nat 100
    , bench "1000" $ nat 1000
    ]
  ]
