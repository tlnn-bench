module Wrapper(naturalNumberAsIntIO) where
import TypeLevel.NaturalNumber(NaturalNumber(..))

naturalNumberAsIntIO :: NaturalNumber n => n -> IO Int
{-# NOINLINE naturalNumberAsIntIO #-}
naturalNumberAsIntIO n = return $! naturalNumberAsInt n
